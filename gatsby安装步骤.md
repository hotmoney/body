



# 第一步

```
git clone -b develop --single-branch https://github.com/junhobaik/junhobaik.github.io.git [输入你的文件夹，不包括[]]
例子：
git clone -b develop --single-branch https://github.com/junhobaik/junhobaik.github.io.git site

```

## 第二步

```
进入{你的文件夹}下运行以下命令
cd （你的文件夹）
```

```
（运行命令安装依赖）
npm install  
```

错误信息

> pngquant-bin@6.0.0 postinstall F:\bokyea\site\node_modules\pngquant-bin
> node lib/install.js

  ‼ Command failed: F:\bokyea\site\node_modules\pngquant-bin\vendor\pngquant.exe
 --version

  ‼ pngquant pre-build test failed
  i compiling from source
  × Error: pngquant failed to build, make sure that libpng-dev is installed
    at F:\bokyea\site\node_modules\bin-build\node_modules\execa\index.js:231:11
    at runMicrotasks (<anonymous>)
    at processTicksAndRejections (internal/process/task_queues.js:97:5)
    at async Promise.all (index 0)
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.13 (node_modules\fs
events):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@
1.2.13: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"
})
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@2.1.3 (node_modules\cho
kidar\node_modules\fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@
2.1.3: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"}
)

npm ERR! code ELIFECYCLE
npm ERR! errno 1
npm ERR! pngquant-bin@6.0.0 postinstall: `node lib/install.js`
npm ERR! Exit status 1
npm ERR!
npm ERR! Failed at the pngquant-bin@6.0.0 postinstall script.
npm ERR! This is probably not a problem with npm. There is likely additional log
ging output above.

npm ERR! A complete log of this run can be found in:
npm ERR!     C:\Users\Administrator\AppData\Roaming\npm-cache\_logs\2021-04-04T1
3_27_15_538Z-debug.log

解决方法

运行以下命令：

npm install --global --production windows-build-tools

**因为Windows不能使用“rm”命令所以需要安装“rimraf模块”来删除“node_modules文件夹“和”package-lock.json文件”后再重新安装**

​	相关链接1：https://stackoverflow.com/questions/26522310/how-to-install-libpng-dev-on-windows

​	相关链接2：https://blog.csdn.net/lewky_liu/article/details/87959880

步骤1：

npm install rimraf -g

步骤2：

rimraf node_modules

步骤3：

rimraf package-lock.json

步骤4：

npm install



### 第三步：

```
npm start
```

再度出现错误：

 ERROR #11331  PLUGIN

问题1：Invalid plugin options for "gatsby-plugin-google-analytics":- "trackingId" is not allowed to be empty。
问题2：warn Warning: there are unknown plugin options for "gatsby-transformer-remark":tableOfContents
Please open an issue at ghub.io/gatsby-transformer-remark if you believe this option is valid.
not finished load plugins - 18.228s
npm ERR! code ELIFECYCLE
npm ERR! errno 1
npm ERR! borderless@3.0.0 start: `gatsby develop`
npm ERR! Exit status 1
npm ERR!
npm ERR! Failed at the borderless@3.0.0 start script.
npm ERR! This is probably not a problem with npm. There is likely additional log
ging output above.

解决问题1方法：

打开"_config.js"文件在google-analytics：一行输入ID。

解决问题2方法：

再度运行：

gatsby develop



**Gatsby相关命令**

gatsby develop  --- 本地运行gatsby

gatsby clean  ---清除缓存

gatsby build  --- 生成静态博客文件



***

* 一些命令使用说明

快速删除node_modules文件夹

前言

当安装了较多模块后，node_modules目录下的文件会很多，直接删除整个目录会很慢，下面介绍些快速删除node_modules目录的方法。

方法一：使用rimraf模块的命令

在全局安装rimraf模块，然后通过其命令来快速删除node_modules目录：

```
npm install rimraf -g
rimraf node_modules
```

方法二：使用命令来删除目录

### Windows下使用rmdir命令

首先在cmd窗口中进入到node_modules文件夹所在的路径，接着执行命令：

```
rmdir /s/q node_modules
```

### Linux下使用rm命令

```
rm -f /node_modules
```

***





知识记录：

node_modules -- 博客需要依赖其文件夹下的模块来运行的的网站，这是保存各种模块的文件夹。

package.json -- 博客需要的模块及版本号都在里面，运行“npm install“后，会按照里面已经写好的模块及模块版本号来安装。

package-lock.json -- 这个文件记录运行“npm install”命令后安装到的模块和模块版本号。

** 不要随便升级任何模块这可能破坏博客 **

既叫模块 = 又可以叫依赖包

***

#### 第四步

将博客推送到gitlab上：

步骤1：

```
git config --global user.name “cyb” 
git config --global user.email “842428188@qq.com”
```

步骤2：

```
git init
git remote add origin git@gitlab.com:hotmoney/body.git
如果提示错误：fatal: remote origin already exists.
最后找到解决办法如下：
1、先删除远程 Git 仓库
git remote rm origin
再：
git remote add origin git@gitlab.com:hotmoney/body.git

```

步骤3：

```
git add .
```

步骤4：

```
git commit -m "Initial commit"
```

步骤5：

git push origin develop



以后每次推送步骤：

git status

git add .

git commit -m ""    ---- “双引号内填写这次更新的说明”

git push origin HEAD

or：

git push origin develop

***



注释：`HEAD`指向当前分支的顶部。`git`可以从中获得分支名称。如果您想要推送与当前分支不同的分支，则命令将无法工作。

参考链接：https://stackoverflow.com/questions/4181861/message-src-refspec-master-does-not-match-any-when-pushing-commits-in-git?page=1&tab=votes#tab-top

***

