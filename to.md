4/1AY0e-g7PzR8hmACqI1PFkKwih5EAtxCI453EiFpyUN1M3wVB6LllGr4I4lg

+  Success! Use this token to login on a CI server:

1//0eSfEg_kllyCVCgYIARAAGA4SNwF-L9Ir53XHblKop7dj04wN3McOYHDTmZPbkvUS_em9OxU9Vvhm
GJQGoTxWpqUaRd2fkme_D2g

Example: firebase deploy --token "$FIREBASE_TOKEN"


Gatsby Firebase Deployment
Having recently updated this very site with Gatsby I wanted to set up a basic pipeline with Gitlab to deploy my site when I push changes to the master branch. This will be beneficial so that if I create another branch, the deploy process won’t initiate on other branches.
盖茨比重火力点部署
在最近用Gatsby更新了这个站点之后，我想用Gitlab建立一个基本的管道，当我将更改推送到主分支时，它就可以部署我的站点。这是有益的，这样如果我创建另一个分支，部署流程就不会在其他分支上启动。
The above gist shows how simple it is to create a basic CI pipeline. Let’s break it down a little though. I only have a deploy task, which uses the latest node container image, then using the script section of the job run npm i to install all the project dependencies. Then we can install some globals, gatsby-cli and firebase-tools, these are used to build the static site and for deployment. To create the site files I would run the gatsby build, and set up firebase to use token authentication. We will come back to this in a minute, and then we run the firebase deploy to… well… deploy the site.
上面的要点显示了创建基本CI管道是多么简单。让我们把它分解一下。我只有一个deploy任务，它使用最新的节点容器映像，然后使用run npm I任务的脚本部分安装所有的项目依赖项。然后，我们可以安装一些全局变量、gatsby-cli和firebase-tools，它们用于构建静态站点和部署。要创建站点文件，我将运行gatsby构建，并设置firebase以使用令牌身份验证。稍后我们会回过头来讨论这个问题，然后我们运行firebase部署来部署站点。
Firebase Token Auth
As we are deploying using CI, we need the deployment to be “hands off” and will need to have an authenticated token for the CI to use to access the project and have permission to deploy. Thankfully firebase has a handy way to generate a token for this exact purpose. If you run the following in your terminal, it will open a browser tab and request you grant access to create this token.
重火力点令牌认证
当我们使用CI进行部署时，我们需要部署是“不干预的”，并且需要有一个经过身份验证的令牌供CI访问项目并拥有部署的权限。值得庆幸的是，firebase提供了一种简便的方法来生成用于此目的的令牌。如果您在终端中运行以下操作，它将打开一个浏览器选项卡，并请求您授予创建此令牌的访问权。
Firebase will then provide you with a handy token within your terminal, which can be used within Gitlab’s CI variables
To use this token we need to add it to Gitlab’s Variables for CI/CD, which can be accessed via Settings > CI / CD:
然后，Firebase将在终端中为您提供一个方便的令牌，它可以在Gitlab的CI变量中使用
要使用这个令牌，我们需要将它添加到Gitlab的CI/CD变量中，可以通过设置> CI/CD访问:
You can see that I have two variables, FIREBASE_TOKEN and PROJECT_ID, these will be injected by Gitlab into the pipeline.
Once you have added the secrets and created the .gitlab-ci.yml file, push to master and this should kick off a build and deploy to firebase! Woop!
您可以看到，我有两个变量FIREBASE_TOKEN和PROJECT_ID，它们将由Gitlab注入到管道中。
一旦您添加了秘密并创建了。gitlab-ci。yml文件，推到主，这应该启动构建和部署到firebase!就跑!
